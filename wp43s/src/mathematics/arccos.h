/* This file is part of 43S.
 *
 * 43S is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 43S is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with 43S.  If not, see <http://www.gnu.org/licenses/>.
 */

/********************************************//**
 * \file arccos.h
 ***********************************************/

void fnArccos   (uint16_t unusedParamButMandatory);
void arccosError(void);
void arccosLonI (void);
void arccosRe16 (void);
void arccosCo16 (void);
void arccosRm16 (void);
void arccosCm16 (void);
void arccosRe34 (void);
void arccosCo34 (void);
