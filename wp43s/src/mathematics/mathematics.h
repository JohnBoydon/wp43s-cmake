/* This file is part of 43S.
 *
 * 43S is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 43S is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with 43S.  If not, see <http://www.gnu.org/licenses/>.
 */

/********************************************//**
 * \file mathematics.h
 ***********************************************/



#include "2pow.h"
#include "10pow.h"
#include "addition.h"
#include "arccos.h"
#include "arcsin.h"
#include "arctan.h"
#include "changeSign.h"
#include "comparisonReals.h"
#include "conjugate.h"
#include "cos.h"
#include "cube.h"
#include "division.h"
#include "exp.h"
#include "factorial.h"
#include "fractionalPart.h"
#include "gamma.h"
#include "gcd.h"
#include "imaginaryPart.h"
#include "integerPart.h"
#include "invert.h"
#include "lcm.h"
#include "ln.h"
#include "log10.h"
#include "log2.h"
#include "magnitude.h"
#include "minusOnePow.h"
#include "multiplication.h"
#include "power.h"
#include "realPart.h"
#include "sign.h"
#include "sin.h"
#include "square.h"
#include "sqrt.h"
#include "subtraction.h"
#include "swapRealImaginary.h"
#include "tan.h"
#include "toPolar.h"
#include "toRect.h"
#include "unitVector.h"
#include "wp34s.h"

