/* This file is part of 43S.
 *
 * 43S is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 43S is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with 43S.  If not, see <http://www.gnu.org/licenses/>.
 */

/********************************************//**
 * \file log10.h
 ***********************************************/

void   fnLog10   (uint16_t unusedParamButMandatory);
void   log10Error(void);
void   log10LonI (void);
void   log10Re16 (void);
void   log10Co16 (void);
void   log10Rm16 (void);
void   log10Cm16 (void);
void   log10ShoI (void);
void   log10Re34 (void);
void   log10Co34 (void);
