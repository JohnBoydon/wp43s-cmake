/* This file is part of 43S.
 *
 * 43S is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 43S is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with 43S.  If not, see <http://www.gnu.org/licenses/>.
 */

/********************************************//**
 * \file 2pow.h
 ***********************************************/

void fn2Pow     (uint16_t unusedParamButMandatory);
void twoPowError(void);
void twoPowLonI (void);
void twoPowRe16 (void);
void twoPowCo16 (void);
void twoPowRm16 (void);
void twoPowCm16 (void);
void twoPowShoI (void);
void twoPowRe34 (void);
void twoPowCo34 (void);
