/* This file is part of 43S.
 *
 * 43S is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 43S is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with 43S.  If not, see <http://www.gnu.org/licenses/>.
 */

/********************************************//**
 * \file magnitude.h
 ***********************************************/

void fnMagnitude   (uint16_t unusedParamButMandatory);
void magnitudeError(void);
void magnitudeLonI (void);
void magnitudeRe16 (void);
void magnitudeCo16 (void);
void magnitudeRm16 (void);
void magnitudeCm16 (void);
void magnitudeShoI (void);
void magnitudeRe34 (void);
void magnitudeCo34 (void);
