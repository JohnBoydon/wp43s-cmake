; Everything after a ; is a comment and is simply ignored
; A line beginning with
;   In:   specifies the input parameters
;   Func: specifies the function to test
;   Out:  specifies the expected output values
;
;   Fn Flag n from 0 to 112: 0 or 1
;   Fx Flag x in A,B,C,D,I,J,K,L,T,X,Y,Z: 0 or 1
;   SL Stack Lift status: 0 or 1
;   IM Integer Mode: 1COMPL, 2COMPL, UNSIGN or SIGNMT
;   AM Angular Mode: DEG, DMS, GRAD, RAD or MULTPI
;   SS Stack Size: 4 or 8
;   WS Word Size: 0 to 64 (0 is the same as 64)
;   RM Rounding mode: 0 to 6
;   SD Significant digits: 0 to 34
;   EC Error Code: 0=no error or form 1 to 28
;   Rn Register n from 0 to 211
;   Rx Register x in A,B,C,D,I,J,K,L,T,X,Y,Z
;      type after equal sign: LonI is a  Long Integer
;                             Re16 is a  Real16 (SP)
;                             Co16 is a  Complex16 (SP)
;                             Angl is an Angle
;                             Stri is a  String
;                             ShoI is a  Short Integer
;                             Re34 is a  Real34 (DP)
;                             Co34 is a  Complex34 (DP)
;      after the colon there is the value of the register between double quotes
;      special value infinity for Re16 and Re34 is "9e9999"
;      special value NaN for Re16 and Re34 is "NaN"
;      The decimal separator is a . or a ,
;      A complex is always in rectangular mode: 2 reals separated by an i example "1.2i-3.4"
;      A short integer is represented as on the calculator: "12345#7"
;      to put a \ or a " in a string use: \\ or \" example: "double quote=\" backslash=\\"
;
; All letters can be written upper or lower case but the function names are case sensitive
;
; No space around the equal sign is allowed
; No space around the colon between the register data type and the value is allowed
; No space around the colon between the register value and the angular mode is allowed


In: FD=0 FI=0 SD=0 RM=0 IM=2compl SS=4 WS=64

In:   SL=0 AM=RAD RX=Re16:"2.2"
Func: fnCos
Out:  EC=0 FI=0 SL=1 RL=Re16:"2.2" RX=Re16:"-0.5885011172553457"

In:   SL=0 AM=DEG RX=Angl:"2.2":RAD
Func: fnCos
Out:  EC=0 FI=0 SL=1 RL=Angl:"2.2":RAD RX=Re16:"-0.5885011172553458"

In:   SL=0 RX=LonI:"2"
Func: fn10Pow
Out:  EC=0 FI=0 SL=1 RL=LonI:"2" RX=LonI:"100"

In:   SL=0 RX=Re16:"2.2"
Func: fn10Pow
Out:  EC=0 FI=0 SL=1 RL=Re16:"2.2" RX=Re16:"158.4893192461113"
